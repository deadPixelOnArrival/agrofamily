


<div class="row main-content col-sm-7">
    <h2>main</h2>

    <div class="products">
        @foreach($products as $product)

            <div class="product">
                <h2>{{ $product->name }}</h2>
                <div class="attributes">
                    <form action="{{ url('/update_product') }}" method="POST" enctype="multipart/form-data">

                        @foreach($product->configurable_attributes as $attribute)
                        <div class="attribute">
                            <h4>{{ $attribute->name }}</h4>
                            <label for="{{ $attribute->name }}">
                                <input type="text" name="{{ $attribute->name }}" value="{{ $attribute->pivot->value }}">
                            </label>
                        </div>
                        @endforeach
                        <input type="submit" class="btn submit_btn" value="predaj se">
                        {{ csrf_field()  }}
                    </form>
                </div>
            </div>
        @endforeach
    </div>
</div>