<?php
namespace Agrofamily\Http\Controllers;

use Agrofamily\Helpers\AttributeHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    public function updateProduct(Request $request, $product_id, $attribute_id, $value)
    {
        $msg='';
        try {
            $attributeHelper = new AttributeHelper();
            $attributeHelper->updateAttributeValue(
                $product_id,
                Auth::user()->id,
                $attribute_id,
                $value
            );
            $msg = 'You have updated the product.';
        } catch (\Exception $e) {
            $msg = 'an error occured while trying to update your product';
        }

        $request->session()
            ->put('message', $msg);
        redirect('/demo');
    }
}
