<?php
namespace Agrofamily\Helpers;

use Illuminate\Support\Facades\DB;


class AttributeHelper
{
    public function updateAttributeValue($product_id, $user_id, $attribute_id, $value){
        $attr = DB::table('attributes_products_users')
            ->where('user_id', $user_id)
            ->where('product_id', $product_id)
            ->where('attribute_id', $attribute_id)
            ->update(['value' => $value]);
    }
}
