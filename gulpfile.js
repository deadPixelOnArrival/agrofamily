var elixir = require('laravel-elixir');

require('laravel-elixir-browserify-official');
require('laravel-elixir-vueify');
require('laravel-elixir-webpack-official');

elixir(function (mix) {
    mix.sass('app.scss');
});

// elixir(function (mix) {
//     mix.webpack('app.js');
// });