<?php


Route::post('/login', [
        'uses' => '\Agrofamily\Http\Controllers\Auth\LoginController@login',
        'as' => 'login'
    ]
);

/*
 * Home Controller
 */
Route::get('/', [
        'uses' => '\Agrofamily\Http\Controllers\HomeController@index',
        'as' => 'home'
    ]
);

Route::get('/demo', [
        'uses' => '\Agrofamily\Http\Controllers\HomeController@demo',
        'as' => 'demo'
    ]
)->middleware('auth');


//TODO remove this route
Route::get('/make', [
        'uses' => '\Agrofamily\Http\Controllers\HomeController@createUser',
        'as' => 'make'
    ]
);

/*
 * Product Controller
 */
Route::post('/update_product', [
        'uses' => '\Agrofamily\Http\Controllers\ProductController@updateProduct',
        'as' => 'update_product'
    ]
)->middleware('auth');
